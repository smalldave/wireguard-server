provider "digitalocean" {
  token = "54571b2bf4abb249344705f0166445cc2cc502e2f38964d98b4ef686deb66972"
}

resource "digitalocean_droplet" "vpn" {
  image = "40214313"
  name = "wireguard-server"
  region = "lon1"
  size = "512mb"
}

output "ip" {
  value = "${digitalocean_droplet.vpn.ipv4_address}"
}
