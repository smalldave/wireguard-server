#!/bin/bash

# Install Wireguard
sudo add-apt-repository -y ppa:wireguard/wireguard
sudo apt-get -y update
sudo apt-get install -y wireguard-dkms wireguard-tools

# Configure firewall
sudo ufw allow 53

# Allow forwarding between adapters so wg0 can go to eth0
echo 'net.ipv4.ip_forward = 1' | sudo tee -a /etc/sysctl.conf

# Copy Wireguard config
sudo cp /tmp/wg0.conf /etc/wireguard
sudo chmod 077 /etc/wireguard/wg0.conf

# Stop services on port 53
echo 'DNSStubListener=no' | sudo tee -a /etc/systemd/resolved.conf
sudo systemctl disable systemd-resolved.service
sudo systemctl stop systemd-resolved
sudo apt-get -y  remove dnsmasq-base
echo "nameserver 8.8.8.8" > /etc/resolv.conf

# Start wire guard service
sudo systemctl enable wg-quick@wg0
sudo systemctl start wg-quick@wg0
sudo systemctl status wg-quick@wg0.service
